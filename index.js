const express = require("express");
const path = require("path");

const app = express();
const port = process.env.PORT || 8080;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/',(req, res) => {
  res.status(200).send({
    message: "lololalu",
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
